const loader = document.querySelector('.loader');
const loadIcon = document.querySelector('.load-icon');

fetch('https://fakestoreapi.com/products')
    .then((result) => {
        return result.json();
    })
    .then((data) => {
        if (data.length == 0) {
            loader.classList.remove('loader');
            loadIcon.classList.remove('load-icon');
            noData();
        } else {
            products(data);
            loader.classList.remove('loader');
            loadIcon.classList.remove('load-icon');
        }
    })
    .catch((err) => {
        console.log(err);
        console.error(err);
        loader.classList.remove('loader');
        loadIcon.classList.remove('load-icon');
        fetchFail();
    });

function products(data) {
    const ul = document.querySelector('ul');
    data.map(product => {
        let li = document.createElement('li');
        let img = document.createElement('img');
        img.src = product.image;
        let h2 = document.createElement('h2');
        h2.innerText = product.title;
        let p = document.createElement('p');
        p.innerText = product.description;
        let p3 = document.createElement('p');
        p3.innerText = product.category;
        let price = document.createElement('h3');
        let div = document.createElement('div');

        div.classList.add("container");
        let p2 = document.createElement('p');
        p2.innerHTML = 'Ratings: ' + product.rating.rate;
        let div2 = document.createElement('div');

        div2.classList.add("price-container");

        let div3 = document.createElement('div');
        div3.classList.add("card");
        let p4 = document.createElement('p');
        let div4 = document.createElement('div');

        div4.classList.add("rating");
        let div5 = document.createElement('div');
        div5.classList.add("category");

        price.innerText = '$' + product.price;
        p4.innerText = 'Reviews: ' + product.rating.count;

        div4.append(p2, p4);
        div5.append(p3, price)
        div2.append(div4, div5);
        div.append(div2);
        div3.append(img, h2, p);

        li.append(div3, div);
        ul.append(li);

    });
}


function fetchFail() {
    let ul = document.querySelector('ul')
    let div = document.createElement('div');

    div.setAttribute('class', 'fetch-error');

    let p = document.createElement('p');
    p.innerText = "failed to fetch the data";

    div.append(p);

    ul.append(div);


}

function noData() {
    let ul = document.querySelector('ul');
    let div = document.createElement('div');

    div.setAttribute('class', 'data-fail');
    let p = document.createElement('p');
    p.innerText = "no data";

    div.append(p);
    ul.append(div);
}