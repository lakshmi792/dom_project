
let form = document.querySelector("form");





function handleSubmit(event) {
    event.preventDefault();
    console.dir(event.target);

    let usernameElm = event.target.elements.firstname;
    let lastnameElm = event.target.elements.lastname;
    let emailAddress = event.target.elements.email;
    let password = event.target.elements.password;
    let repeatPassword = event.target.elements.confirm;
    let checkBox = event.target.elements.checkbox;


    if (firstName(usernameElm)) {
        usernameElm.classList.add("success");
        usernameElm.classList.remove("error");
        usernameElm.nextElementSibling.style.display = "none";
    } else {
        usernameElm.classList.add("error");
        usernameElm.classList.remove("success");
        usernameElm.nextElementSibling.style.display = "block";
    }


    if (lastName(lastnameElm)) {
        lastnameElm.classList.add("success");
        lastnameElm.classList.remove("error");
        lastnameElm.nextElementSibling.style.display = "none";
    } else {
        lastnameElm.classList.add("error");
        lastnameElm.classList.remove("success");
        lastnameElm.nextElementSibling.style.display = "block";
    }


    /**email validation */
    if (validateEmail(emailAddress)) {
        emailAddress.classList.add("success");
        emailAddress.classList.remove("error");
        email.nextElementSibling.style.display = "none";
    } else {
        emailAddress.classList.add("error");
        emailAddress.classList.remove("success");
        emailAddress.nextElementSibling.style.display = "block";
    }

    if (checkPassword(password)) {
        password.classList.add("success");
        password.classList.remove("error");

    } else {
        password.classList.add("error");
        password.classList.remove("success");
        password.nextElementSibling.style.display = "block";
    }

    if (confirmPassword(password, repeatPassword)) {
        repeatPassword.classList.add("success");
        repeatPassword.classList.remove("error");
    } else {
        repeatPassword.classList.add("error");
        repeatPassword.classList.remove("success");
        repeatPassword.nextElementSibling.style.display = "block";
    }

    /** */
    if (checkBox.checked) {
        let checkBoxParent = checkBox.parentElement;
        // count++;
        checkBoxParent.children[2].style.display = "none";

    } else {
        let checkBoxParent = checkBox.parentElement;
        checkBoxParent.children[2].textContent = "Please accept terms";
        checkBoxParent.children[2].style.display = "block";
    }



}


form.addEventListener("submit", handleSubmit);

function firstName(usernameElm) {
    let trimValue = usernameElm.value.trim();
    let userNameError = "";
    if (trimValue === "") {
        userNameError = "Can't be empty!";
        usernameElm.nextElementSibling.textContent = userNameError;
        return false;
    } else if (doesContainANumber(trimValue)) {
        userNameError = "Must not contain a number";
        usernameElm.nextElementSibling.textContent = userNameError;
        return false;
    } else if (specialCharacter(trimValue)) {
        userNameError = "Must not contain special characters";
        usernameElm.nextElementSibling.textContent = userNameError;
        return false;
    } else {
        return true;
    }
}

function specialCharacter(value) {
    return (/[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/).test(value);
}
function doesContainANumber(str) {
    return str.split('').some(element => Number(element));
}

function lastName(lastnameElm) {
    let trimValue = lastnameElm.value.trim();
    let lastNameError = "";
    if (trimValue === "") {
        lastNameError = "Can't be empty!";
        lastnameElm.nextElementSibling.textContent = lastNameError;
        return false;
    } else if (doesContainANumber(trimValue)) {
        lastNameError = "Must not contain a number";
        lastnameElm.nextElementSibling.textContent = lastNameError;
        return false;
    } else if (specialCharacter(trimValue)) {
        lastNameError = "Must not contain special characters";
        lastnameElm.nextElementSibling.textContent = lastNameError;
        return false;
    } else {
        return true;
    }
}


function validateEmail(email) {
    let emailError = "";
    const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    if (email.value.match(validRegex)) {
        return true;

    } else {
        if (email.value === "") {
            emailError = "Invalid email";
            email.nextElementSibling.textContent = emailError;
            return false;
        }
        else if (email.value.includes('@') === false) {
            emailError = "Invalid email";
            email.nextElementSibling.textContent = emailError;
            return false;
        }
        else if (email.value.includes('.') === false) {
            emailError = "Invalid email";
            email.nextElementSibling.textContent = emailError;
            return false;
        }
        else if ((/[`!#$%^&*()_+\-=\[\]{};':"\\|,<>\/?~]/).test(email.value)) {
            emailError = "Invalid email";
            email.nextElementSibling.textContent = emailError;
            return false;
        }

    }

}



function checkPassword(input) {
    let password = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
    if (input.value.match(password)) {
        return true;
    } else {
        let passwordError = "Invalid password";
        input.nextElementSibling.textContent = passwordError;
        return false;

    }

}

function confirmPassword(password, confirm) {
    let confirmPasswordError = "";
    if (password.value !== confirm.value) {
        confirmPasswordError = "password is not matched";
        confirm.nextElementSibling.textContent = confirmPasswordError;
        return false;
    } else if (confirm.value === "") {
        confirmPasswordError = "It can't be empty!";
        confirm.nextElementSibling.textContent = confirmPasswordError;
        return false;
    } else {
        return true;

    }
}








